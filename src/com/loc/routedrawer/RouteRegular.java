/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loc.routedrawer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.loc.structures.AreaRoute;
import com.loc.structures.Point2D;
import com.loc.structures.Segment;

/**
 *
 * @author bigstone
 */
public class RouteRegular {

    public void regularizeRoute(List<LinkedList<Segment>>  routes) {
    	for (LinkedList<Segment> route : routes) {
//    		List<Segment> route = areaRoute.routes;
    		this.regularizeTouchPoint(route);
            this.regularizeIntersection(route);
            this.regularizeVertice(route);
		}
    }

    public void regularizeVertice(List<Segment> route) {
        int r = Point2D.RADIUS;
        List<Point2D> pList = new ArrayList<Point2D>();
        List<Point2D> tmp = new ArrayList<Point2D>();
        for (Segment s : route) {
            pList.add(s.ePoint);
            pList.add(s.sPoint);
        }
        
        while (!pList.isEmpty()) {
            tmp.clear();
            Point2D p = pList.remove(0);
            for (int i = pList.size() - 1; i >= 0; i--) {
                if (p.distance(pList.get(i)) <= r) {
                    tmp.add(pList.remove(i));
                }
            }
            tmp.add(p);
            mergePoints(tmp);
        }
    }
    private void mergePoints(List<Point2D> tmp){
        float xt = 0;
        float yt = 0;
        for (Point2D p : tmp) {
            xt += p.x;
            yt += p.y;
        }
        xt /= tmp.size();
        yt /= tmp.size();
        for (Point2D p : tmp) {
            p.x = xt;
            p.y = yt;
        }
    }
    public void regularizeChain(List<Segment> route) {
        int r = Point2D.RADIUS;
        for (Segment s : route) {
            for (Segment s1 : route) {
                if (s1 != s && s.isChained(s1)) {
                    if (s1.sPoint.isNearTo(s.sPoint, r)) {
                        setMidPoint(s.sPoint, s1.sPoint);
                    }

                    if (s1.ePoint.isNearTo(s.sPoint, r)) {
                        setMidPoint(s.sPoint, s1.ePoint);
                    }

                    if (s1.sPoint.isNearTo(s.ePoint, r)) {
                        setMidPoint(s.ePoint, s1.sPoint);
                    }

                    if (s1.ePoint.isNearTo(s.ePoint, r)) {
                        setMidPoint(s.ePoint, s1.ePoint);
                    }
                }
            }
        }
    }

    private void setMidPoint(Point2D p1, Point2D p2) {
        float mx = (p1.x + p2.x) / 2;
        float my = (p1.y + p2.y) / 2;
        p1.x = mx;
        p1.y = my;
        p2.x = mx;
        p2.y = my;
    }

    public void regularizeTouchPoint(List<Segment> route) {

        if (route.isEmpty()) {
            return;
        }
        float r = Point2D.RADIUS;
        List<Segment> regRoute = new LinkedList<Segment>();
        Segment s = route.remove(0);

        while (!route.isEmpty()) {
            boolean isReg = true;
            for (int i = 0; i < route.size(); i++) {
                Segment s1 = route.get(i);
                if (!s1.isChained(s)) {
                    isReg = false;
                    if (s1.distanceLine(s.sPoint) <= r) {
                        Point2D pPoint = s1.projectPoint(s.sPoint);
                        s.sPoint.x = pPoint.x;
                        s.sPoint.y = pPoint.y;
                        List<Segment> l = spliteSegmentAtPoint(s1, pPoint);
                        route.addAll(l);
                        route.remove(s1);
                        break;
                    }
                    if (s1.distanceLine(s.ePoint) <= r) {
                        Point2D pPoint = s1.projectPoint(s.ePoint);
                        s.ePoint.x = pPoint.x;
                        s.ePoint.y = pPoint.y;
                        List<Segment> l = spliteSegmentAtPoint(s1, pPoint);
                        route.addAll(l);
                        route.remove(s1);
                        break;
                    }

                    if (s.distanceLine(s1.sPoint) <= r) {
                        Point2D pPoint = s.projectPoint(s1.sPoint);
                        s1.sPoint.x = pPoint.x;
                        s1.sPoint.y = pPoint.y;
                        List<Segment> l = spliteSegmentAtPoint(s, pPoint);
                        s = l.get(0);
                        route.add(l.get(1));
                        break;
                    }
                    if (s.distanceLine(s1.ePoint) <= r) {
                        Point2D pPoint = s.projectPoint(s1.ePoint);
                        s1.ePoint.x = pPoint.x;
                        s1.ePoint.y = pPoint.y;
                        List<Segment> l = spliteSegmentAtPoint(s, pPoint);
                        s = l.get(0);
                        route.add(l.get(1));
                        break;
                    }
                    isReg = true;
                }
            }
            if (isReg) {
                regRoute.add(s);
                s = route.remove(0);
            }
        }
        route.add(s);
        route.addAll(regRoute);
    }

    public void regularizeIntersection(List<Segment> route) {
        if (route.isEmpty()) {
            return;
        }
        List<Segment> regRoute = new LinkedList<Segment>();
        Segment s = route.remove(0);
        while (!route.isEmpty()) {
            boolean isReg = true;
            for (int i = 0; i < route.size(); i++) {
                Segment s1 = route.get(i);
                if (!s1.isChained(s)) {
                    isReg = false;
                    Point2D intersection = s.intersectPoint(s1);
                    if (intersection != null) {
                        List<Segment> l = spliteSegmentAtPoint(s, intersection);
                        s = l.remove(0);
                        route.add(l.remove(0));
                        l = spliteSegmentAtPoint(s1, intersection);
                        route.addAll(l);
                        route.remove(s1);
                        break;
                    }
                    isReg = true;
                }
            }
            if (isReg) {
                regRoute.add(s);
                s = route.remove(0);
            }
        }
        route.add(s);
        route.addAll(regRoute);
    }

    private List<Segment> spliteSegmentAtPoint(Segment s, Point2D p) {
        List<Segment> l = new ArrayList<Segment>();
        l.add(new Segment(s.sPoint, new Point2D(p)));
        l.add(new Segment(s.ePoint, new Point2D(p)));
        return l;
    }
}
