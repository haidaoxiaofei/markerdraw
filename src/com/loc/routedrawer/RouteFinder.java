/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.loc.routedrawer;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import com.loc.structures.AreaRoute;
import com.loc.structures.Point2D;
import com.loc.structures.Segment;

/**
 *
 * @author bigstone
 */
public class RouteFinder {
    public Segment findNearestSegment(List<Segment> l, Point2D p){
        Segment sTmp = null;
        float disMin = Float.MAX_VALUE;
        for (Segment s : l) {
            float disTmp = s.distanceLine(p);
            if (disTmp < disMin) {
                disMin = disTmp;
                sTmp = s;
            }
        }
        return sTmp;
    }
    private List<Segment> extractChainedRoutes(List<Segment> rList, Segment cRoute){
        List<Segment> chainedRoutes = new LinkedList<Segment>();
        for (int i = rList.size() - 1; i >= 0; i--) {
            if (rList.get(i).isChained(cRoute)) {
                chainedRoutes.add(rList.remove(i));
            }
        }
        return chainedRoutes;
    }
    public Stack<Segment> findRoute(AreaRoute areaRoute, Point2D sPoint, Point2D ePoint){
        List<Segment> rList = new LinkedList<Segment>(areaRoute.routes);
        Stack<Segment> route = new Stack<Segment>();

        Segment cRoute = findNearestSegment(rList, sPoint);
        Segment eRoute = findNearestSegment(rList, ePoint);
        rList.remove(cRoute);

        while (!rList.isEmpty()) {
            if (cRoute.equals(eRoute)) {
                route.push(cRoute);
                break;
            }
            List<Segment> chainedRoutes = extractChainedRoutes(rList, cRoute);
            if (chainedRoutes.isEmpty()) {
                if (route.isEmpty()) {
                    break;
                }
                cRoute = route.pop();
            } else {
                route.push(cRoute);
                cRoute = findNearestSegment(chainedRoutes, ePoint);
                chainedRoutes.remove(cRoute);
                rList.addAll(chainedRoutes);
            }
        }
        return route;        
    }
    
    
}
