package com.loc.routedrawer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loc.structures.Marker;
import com.loc.structures.Point2D;
import com.loc.structures.Point2D.PointType;
import com.loc.structures.Segment;

public class MainActivity extends Activity {

	private ImageView imageView;
	private int imageIndex = 0;
	private int location = 0;
	private TextView text;
	private static int COMPRESS_RATE = 2;
	private MultiPointTouchListener mtpl;
	private OnTouchListener pointListener;
	String imgFilePath;
	String filename;
	private String  locDescription = "";
	private PointType pointType;
	private Spinner category;

	private Canvas canvas;
	private Paint paint;
	Boolean flag = false;

	private float x, y;

	private Point2D sPoint = new Point2D();
	private Point2D ePoint = new Point2D();
	// private List<Segment> segments = new LinkedList<Segment>();
	private List<LinkedList<Segment>> segmentsList = new ArrayList<LinkedList<Segment>>();
	private List<LinkedList<Marker>> markersList = new ArrayList<LinkedList<Marker>>();
	
	private static String PRE_PATH = Environment.getExternalStorageDirectory()
			+ "/wherami";
	String routeFilePath = Environment.getExternalStorageDirectory()
			.getPath()+ "/" + "route.txt";
	String markerFilePath = Environment.getExternalStorageDirectory()
			.getPath()+ "/" + "marker.txt";
	private String floorNameArray[] = { "Ground", "Floor1", "Floor2", "Floor3",
			"Floor4", "Floor5", "Floor6", "Floor7", "LG1", "LG2", "LG4", "LG5",
			"LG7", "President Lodge" };
	private List<String> fileNameArray = new ArrayList<String>();

	private static boolean isExit = false;
	private Segment chosenSegment = null;
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// setContentView(new SampleView(this));

		imageView = (ImageView) findViewById(R.id.imageView);
		category = (Spinner) findViewById(R.id.category);

		// setImageView(imageIndex);
		text = (TextView) findViewById(R.id.text);
		mtpl = new MultiPointTouchListener();
		imageView.setOnTouchListener(mtpl);

		pointListener = new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				float[] point = new float[2];
				point[0] = event.getX();
				point[1] = event.getY();

				Matrix matrix = new Matrix();
				if (imageView.getImageMatrix().invert(matrix)) {
					matrix.mapPoints(point);
					x = point[0] * COMPRESS_RATE;
					y = point[1] * COMPRESS_RATE;
					switch (pointType) {
					case START:
						sPoint.x = x;
						sPoint.y = y;
						drawPoint(point[0], point[1], PointType.START);
						break;
					case END:
						ePoint.x = x;
						ePoint.y = y;
						drawPoint(point[0], point[1], PointType.END);
						segmentsList.get(location).add(
								new Segment(new Point2D(sPoint), new Point2D(
										ePoint)));
						drawRoute(segmentsList.get(location).get(
								segmentsList.get(location).size() - 1));
						break;
					case MARK:
						makeDescription();
						
						
						break;
					case SEGMENT:
						drawRoute(chosenSegment);
						Point2D p = new Point2D(x,y);
						chosenSegment = findBestSegment(segmentsList.get(location), p);
//						segmentsList.get(location).remove(chosenSegment);
						drawChosenRoute(chosenSegment);
						break;
					}

					text.setText("X:" + x + "|Y:" + y);
				} else {
					text.setText("Matrix is not invertible?");
				}

				imageView.setOnTouchListener(mtpl);
				return true;
			}

		};
		
		//load record
		for (int i = 0; i < floorNameArray.length; i++) {
			segmentsList.add(new LinkedList<Segment>());
		}
		for (int i = 0; i < floorNameArray.length; i++) {
			markersList.add(new LinkedList<Marker>());
		}
		try {
			loadRouteRecord();
			loadMarkerRecord();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		File ff = new File(PRE_PATH);
		List<String> flist = new ArrayList<String>();
		if (ff.exists()) {
			flist = getListOnSys(ff);
		}

		ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, flist);
		// 设置下拉列表风格
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// 将adapter添加到spinner中
		category.setAdapter(adapter);
		// 添加Spinner事件监听
		category.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view,
					int i, long l) {
				String item = (String) adapterView.getSelectedItem();
				location = adapterView.getSelectedItemPosition();
				Toast.makeText(getApplicationContext(), item, Toast.LENGTH_LONG)
						.show();

				setImageView(item.split(",")[1].trim());
				imageView.setOnTouchListener(mtpl);
			}
		});

		
	}

	Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			isExit = false;
		}
	};

	/**
	 * @author function 用于扫描SD卡上的文件
	 * 
	 */

	public List<String> getListOnSys(File file) {
		// 从根目录开始扫描
		Log.i("filescan", file.getPath());
		// HashMap<String, String> fileList = new HashMap<String, String>();
		List<String> fileList = new ArrayList<String>();
		getFileList(file, fileList);
		return fileList;
	}

	/**
	 * @param path
	 * @param fileList
	 *            注意的是并不是所有的文件夹都可以进行读取的，权限问题
	 */
	private void getFileList(File path, List<String> fileList) {
		// 如果是文件夹的话
		if (path.isDirectory()) {
			// 返回文件夹中有的数据
			File[] files = path.listFiles();
			// 先判断下有没有权限，如果没有权限的话，就不执行了
			if (null == files) {
				Log.i("filescan", "null or not permited");
				return;
			}

			for (int i = 0; i < files.length; i++) {
				getFileList(files[i], fileList);
			}
		}
		// 如果是文件的话直接加入
		else {
			if (path.getAbsolutePath().endsWith("jpg")) {
				Log.i("filescan", path.getAbsolutePath());
				// 进行文件的处理
				String filePath = path.getAbsolutePath();
				// 文件名
				String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
				// 添加
				filePath = filePath.substring(filePath.indexOf(PRE_PATH)
						+ PRE_PATH.length());

				filename = filePath.substring(0, filePath.lastIndexOf("/"));
				filename = filename.substring(filename.lastIndexOf("/") + 1);
				imageIndex = Integer.valueOf(filename) - 1000;

				fileList.add(floorNameArray[imageIndex] + " ," + filePath);
				fileNameArray.add(filePath);
			}
		}
	}

	private void setImageView(String path) {
		imgFilePath = PRE_PATH + path.trim();
		Log.i("imgFilePath", imgFilePath);
		filename = imgFilePath.substring(0, imgFilePath.lastIndexOf("/"));
		filename = filename.substring(filename.lastIndexOf("/") + 1);
		imageIndex = Integer.valueOf(filename) - 1000;
		
		Bitmap bmp = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = COMPRESS_RATE;
		bmp = BitmapFactory.decodeFile(imgFilePath, options).copy(
				Bitmap.Config.ARGB_8888, true);
		
		canvas = new Canvas(bmp);
		paint = new Paint();
		paint.setStyle(Style.STROKE);
		canvas.drawBitmap(bmp, 0, 0, paint);

		paint.setAntiAlias(true);
		List<Segment> segments = segmentsList.get(location);
		for (Segment s : segments) {
			drawRoute(s);
			
		}
		List<Marker> markers = markersList.get(location);
		for (Marker m : markers) {
			drawMarker(m);
		}
		imageView.setImageBitmap(bmp);
	}
	public Segment findBestSegment(List<Segment> l, Point2D p){
        Segment sTmp = null;
        float disMin = Float.MAX_VALUE;
        for (Segment s : l) {
            float disTmp = s.distanceLine(p);            
            if (disTmp < disMin) {
                disMin = disTmp;
                sTmp = s;
            }
        }
        return sTmp;
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onSPoint(View v) {
		pointType = PointType.START;
		imageView.setOnTouchListener(pointListener);

	}
	public void onChooseSegment(View v) {
		pointType = PointType.SEGMENT;
		imageView.setOnTouchListener(pointListener);
	}
	public void onDeleteSegment(View v) {
		if(chosenSegment == null)return;
		segmentsList.get(location).remove(chosenSegment);
		chosenSegment = null;
		String path = fileNameArray.get(location);
		setImageView(path);
		imageView.invalidate();
	}
	
	public void onEPoint(View v) {
		pointType = PointType.END;
		imageView.setOnTouchListener(pointListener);
	}
	public void onMark(View v){
		pointType = PointType.MARK;
		imageView.setOnTouchListener(pointListener);
	}
	public void onDemark(View v){
		
		if (markersList.get(location).isEmpty()) {
			return;
		}
		markersList.get(location).remove(markersList.get(location).size() - 1);
		String path = fileNameArray.get(location);
		setImageView(path);
		imageView.invalidate();
        
	}
	
	private void makeDescription(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Title");

		// Set up the input
		final EditText input = new EditText(this);
		// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		    	locDescription = input.getText().toString();
		    	Marker m = new Marker(new Point2D(x,y), location, locDescription,15);
		    	markersList.get(location).add(m);
				drawMarker(m);
		    }
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        dialog.cancel();
		    }
		});

		builder.show();
	}
	
	 
	public void onSave(View v) {
		saveRouteRecord();
		saveMarkerRecord();
		Toast.makeText(getApplicationContext(), "信息已保存", Toast.LENGTH_LONG)
				.show();
	}

	public void onCancel(View v) {
		
		List<Segment> segments = segmentsList.get(location);
		if (segments.isEmpty()) {
			return;
		}
		segments.remove(segments.size() - 1);
		String path = fileNameArray.get(location);
		setImageView(path);
		imageView.invalidate();
	}
	

	private void drawPoint(double x, double y, PointType type) {
		if (type == PointType.START) {
			paint.setColor(Color.RED);
		}

		if (type == PointType.END) {
			paint.setColor(Color.MAGENTA);
		}
		paint.setStrokeWidth(2);
		canvas.drawPoint((float) x, (float) y, paint);
		imageView.invalidate();

	}
	private void drawMarker(Marker m){
		paint.setColor(Color.RED);
		
		canvas.drawCircle(m.loc.x/COMPRESS_RATE, m.loc.y/COMPRESS_RATE, m.radius, paint);
		imageView.invalidate();
	}
	
	private void drawRoute(Segment s) {
		if(s == null)return;
		paint.setColor(Color.RED);
		canvas.drawLine(s.sPoint.x / COMPRESS_RATE, s.sPoint.y / COMPRESS_RATE,
				s.ePoint.x / COMPRESS_RATE, s.ePoint.y / COMPRESS_RATE, paint);
		imageView.invalidate();
	}
	private void drawChosenRoute(Segment s) {
		if(s == null)return;
		paint.setColor(Color.BLUE);
//		paint.setStrokeWidth(4);
		canvas.drawLine(s.sPoint.x / COMPRESS_RATE, s.sPoint.y / COMPRESS_RATE,
				s.ePoint.x / COMPRESS_RATE, s.ePoint.y / COMPRESS_RATE, paint);
		imageView.invalidate();
	}
	private void loadRouteRecord() throws NumberFormatException, IOException{
		FileReader reader = new FileReader(routeFilePath);
        BufferedReader br = new BufferedReader(reader);
        String rtRecord = null;
        while ((rtRecord = br.readLine()) != null) {
            String rtInfo[] = rtRecord.split(" ");
            Point2D point1 = new Point2D(rtInfo[1], rtInfo[2]);
            Point2D point2 = new Point2D(rtInfo[3], rtInfo[4]);
            
            int loc = Integer.valueOf(rtInfo[0].trim());
            segmentsList.get(loc).add(new Segment(point1,point2));
        }
        reader.close();
	}
	
	private void loadMarkerRecord() throws NumberFormatException, IOException{
		FileReader reader = new FileReader(markerFilePath);
        BufferedReader br = new BufferedReader(reader);
        String rtRecord = null;
        while ((rtRecord = br.readLine()) != null) {

            String rtInfo[] = rtRecord.split(",");
            if(rtInfo.length < 5){
            	continue;
            }
            Point2D position = new Point2D(rtInfo[1], rtInfo[2]);
            int radius = Integer.valueOf(rtInfo[3]);
            
            int loc = Integer.valueOf(rtInfo[0].trim());
            markersList.get(loc).add(new Marker(position, loc,rtInfo[4], radius));
        }

        reader.close();
	}
	private void saveMarkerRecord() {
		
		try {
			
			FileWriter fw = new FileWriter(markerFilePath);
			for (List<Marker> areaMarkers : markersList) {
				for (Marker m : areaMarkers) {
					fw.write(m.toString());
				}
			}
			
			fw.flush();
			fw.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}
	private void saveRouteRecord() {
		RouteRegular r = new RouteRegular();
		r.regularizeRoute(segmentsList);
		try {
			
			FileWriter fw = new FileWriter(routeFilePath);
			for (int i = 0; i < segmentsList.size(); i++) {
				List<Segment> linkedList = segmentsList.get(i);
				for (Segment segment : linkedList) {
					fw.write(""+i+" "+segment+"\n");
				}
			}
			fw.flush();
			fw.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exit();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	private void exit() {
		if (!isExit) {
			isExit = true;
			Toast toast = Toast.makeText(getApplicationContext(), "再按一次退出程序",
					Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			// 利用handler延迟发送更改状态信息
			mHandler.sendEmptyMessageDelayed(0, 2000);
		} else {
			finish();
			System.exit(0);
		}
	}

}
