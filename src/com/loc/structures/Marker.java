package com.loc.structures;

public class Marker {
	public Point2D loc;
	public int areaId;
	public String description;
	public int radius;
	public Marker(Point2D loc, int areaId, String description, int radius) {
		super();
		this.loc = loc;
		this.areaId = areaId;
		this.description = description;
		this.radius = radius;
	}
	@Override
	public String toString() {
		return "" + areaId + "," + loc.x + "," + loc.y + ","
				+ radius + "," + description+"\n";
	}
	
}
