/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.loc.structures;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author bigstone
 */
public class AreaRoute {
    private final int areaId;
    public List<Segment> routes = new LinkedList<Segment>();

    public AreaRoute(int areaId) {
        this.areaId = areaId;
    }

    public int getAreaId() {
        return areaId;
    }
    
    
}
