package com.ust.cse.fp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.util.ByteArrayBuffer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.ust.cse.structures.AreaPoint;
import com.ust.cse.utils.FloorConfig;

import hk.ust.cse.gmission.R;

public class MainActivity extends Activity {
	private RelativeLayout main;
	private ImageView marker;
	private ImageView imageView;
	private int areaId = -1;
	private TextView text;
	private static int COMPRESS_RATE = 4;
	private MultiPointTouchListener mtpl;
    String imgFilePath;
    String filename;
    WifiManager.WifiLock wifiLock;
	
	private Button pickButton;
	private Button startButton;
    private Spinner category;
    private Canvas canvas;
    private Paint paint;
    Boolean flag = false;

	private float x, y;
    private float xx, yy;

    private boolean AP_FILTERING = true;
    private boolean GRID;

	private static final int MARKER_WIDTH = 40;
	private static final int MARKER_HEIGHT = 40;
	
	private float scale = 13;// scale is the count of  pixels per meter.

	private static int scanCount = 0;

	private WifiManager wifiManager;
	private long lastWiFiTS;// timestamp for last wifi record
	private boolean start_record;
	
	private static int TARGET_SCAN_TIMES = 10;
    private static String PRE_PATH = "/sdcard/gmission_data/.map/";
    private static String OUTPUT_BASE_PATH = "/sdcard/gmission_data/fingerprints/";
    

//    private static final String[] wifi_names = {"sMobileNet","Universities WiFi","Y5ZONE","Alumni","eduroam","PCCW"};


	private List<ArrayList<Integer>> signals = new ArrayList<ArrayList<Integer>>();
	private List<Fingerprint> fingerprints = new ArrayList<Fingerprint>();
	private Map<String, Integer> macMap = new HashMap<String, Integer>();
	private int count = 0;//ap count
    public String districtName;
    public String[] floorNames;
    public String[] floorFolders;
    public String[]	wifiNames;
    public float CONVERT_DEGREE = 0;
    private static boolean isExit = false;

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isExit = false;
        }
    };


	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        //setContentView(new SampleView(this));

		loadFloorConfig();
        

		pickButton = (Button) findViewById(R.id.action_pick);
		startButton = (Button) findViewById(R.id.action_start);
		imageView = (ImageView) findViewById(R.id.imageView);
        category = (Spinner) findViewById(R.id.category);

        //setImageView(imageIndex);
		text = (TextView) findViewById(R.id.text);
		mtpl = new MultiPointTouchListener();
		imageView.setOnTouchListener(mtpl);
		main = new RelativeLayout(this);
		marker = new ImageView(this);

        RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(MARKER_WIDTH,MARKER_HEIGHT);
        parms.setMargins(-100,-100, 0,0);
        marker.setLayoutParams(parms);
        marker.setBackgroundResource(R.drawable.marker);
        main.addView(marker);
        this.addContentView(main, new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
		MultiPointTouchListener.marker = marker;


       
		setupWIFI();
        
		setupAdapter();
        
      //watch dog for WiFi
  		new Thread(new Runnable() {
  	        @Override
  	        public void run() {
  	            while (true) {
  	                try {
  	                    Thread.sleep(1000);
  	                    mHandler.post(new Runnable() {
  	                        @Override
  	                        public void run() {
  	                        	long current = System.currentTimeMillis();
  	                        	if(start_record && current - lastWiFiTS >= 3000){
  	                        	
  	                        	wifiManager.startScan();
  	                        	}
  	                        }
  	                    });
  	                } catch (Exception e) {
  	                }
  	            }
  	        }
  	    }).start();
    }
	
	private void changeFocusedDistrict(int focusedDistrict){
		areaId = -1; // reset areaId
		FloorConfig.focusedDistrict = focusedDistrict;
		loadFloorConfig();
		setupAdapter();
	}
	
	private void loadFloorConfig(){
        AssetManager assetManager = this.getAssets();
		FloorConfig floorConfig = FloorConfig.getInstance();
        try{
            floorConfig.reloadConfig(assetManager.open("AreaConfig.json"));
        } catch (Exception e){
            e.printStackTrace();
        }

        districtName = floorConfig.districtName;
        floorNames = floorConfig.floorNames;
        floorFolders = floorConfig.floorFolders;
        wifiNames = floorConfig.wifiNames;
        COMPRESS_RATE = floorConfig.compress_rate;
        GRID = floorConfig.GRID;
        CONVERT_DEGREE = floorConfig.convertDegree;
	}
	
	private void setupWIFI(){
		 wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

			registerReceiver(new BroadcastReceiver() {
				public void onReceive(Context context, Intent intent) {
					lastWiFiTS = System.currentTimeMillis();
	                if(start_record){
	                    List<ScanResult> results = wifiManager.getScanResults();
	                    for (ScanResult result : results) {
	                        if (AP_FILTERING && !isFocus(result.SSID)) {
	                            continue;
	                        }

	                        if (macMap.containsKey(result.BSSID)) {
	                            Integer index = (Integer) macMap.get(result.BSSID);
	                            signals.get(index).add(result.level);
	                        } else {
	                            macMap.put(result.BSSID, count);
	                            signals.add(new ArrayList<Integer>());
	                            signals.get(count).add(result.level);
	                            count++;
	                        }
	                    }
	                    scanCount++;
	                    if (scanCount <TARGET_SCAN_TIMES) {
	                        System.out.println("size scanCount:"+scanCount);
	                        startButton.setText("Countdown:"+ (10-scanCount));
	                        wifiManager.startScan();
	                    } else {
	                        Date now = new Date();
	                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd-HH:mm:ss");
	                        String date = dateFormat.format(now);
	                        double medianSignals[] = new double[macMap.size()];
	                        String macList[] = new String[macMap.size()];
	                        Set<String> macs = macMap.keySet();

	                        for (String mac : macs) {
	                            int index = macMap.get(mac);
	                            macList[index] = mac;
	                            List<Integer> tmp = signals.get(index);
	                            Collections.sort(tmp);
	                            if (tmp.size() > 0) {
	                                if (tmp.size() % 2 == 1) {
	                                    medianSignals[index] = tmp.get(tmp.size() / 2);
	                                } else {
	                                    medianSignals[index] = (tmp.get(tmp.size() / 2) + tmp
	                                            .get((tmp.size() - 1) / 2)) / 2.0;
	                                }
	                            }
	                            tmp.clear();
	                        }

	                        fingerprints.add(new Fingerprint(areaId, date, new AreaPoint((int)x, (int)y, areaId), macList, medianSignals));
	                        releaseLock();
	                        pickButton.setEnabled(true);
	                        startButton.setEnabled(true);
	                        startButton.setText("Collect");
	                        start_record = false;
	                    }
	                }
				}
			}, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
			this.start_record = false;
	}
	
	private void setupAdapter(){
		ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,floorNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(adapter);
        category.setOnItemSelectedListener(new Spinner.OnItemSelectedListener()
        {
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int index = adapterView.getSelectedItemPosition();
                if(areaId != index){
                	setImageView(floorFolders[index]);
                	MultiPointTouchListener.isMarkered = false;
                }
                areaId = index;
                imageView.setOnTouchListener(mtpl);
            }
        });
	}
	
    public WifiLock createWifiLock(String lockName)
    {
        wifiLock = wifiManager.createWifiLock(lockName);
        return wifiLock;
    }

    public WifiLock createWifiLock(String lockName, int lockType)
    {
        wifiLock = wifiManager.createWifiLock(lockType, lockName);
        return wifiLock;
    }

    public void lockWifi()
    {
        wifiLock.acquire();
    }

    public void releaseLock()
    {
        if (wifiLock.isHeld())
        {
            wifiLock.release();
            Log.i("wifilock","down");
        }
    }

    public boolean isHeld()
    {
        return wifiLock.isHeld();
    }

    private void setImageView(String floorFolderName){
		imgFilePath = PRE_PATH + floorFolderName+"/map.jpg";
        Log.i("Pic Path", imgFilePath);
		File mapFile = new File(imgFilePath);
		if(!mapFile.exists()){
			mapFile.getParentFile().mkdirs();
			new RemoteMapRetrieveTask().execute(floorFolderName, imgFilePath);
		} else {
			Bitmap myImg = null;
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = COMPRESS_RATE;
			myImg = BitmapFactory.decodeFile(imgFilePath, options).copy(Bitmap.Config.ARGB_8888, true);
            Matrix matrix = new Matrix();
            matrix.postRotate(CONVERT_DEGREE);

            Bitmap rotated = Bitmap.createBitmap(myImg, 0, 0, myImg.getWidth(), myImg.getHeight(),
                    matrix, true);
	        int WIDTH = 20;
	        int HEIGHT = 20;
	        canvas = new Canvas(rotated);
	        float w = rotated.getWidth();
	        float h = rotated.getHeight();
	        int xCount=(int)w/WIDTH;
	        int yCount=(int)h/HEIGHT;
	        paint=new Paint();
	        canvas.drawBitmap(rotated, 0, 0, paint);
	        paint.setStyle(Paint.Style.STROKE);
	        paint.setStrokeWidth(1);
	        paint.setColor(Color.BLUE);
	        paint.setAlpha(50);
	        
	        if(COMPRESS_RATE >= 2){
	        	for(int i=0;i<xCount;i++){
		            for(int j=0;j<yCount;j++){
		                canvas.drawRect(i*WIDTH, j*HEIGHT,
		                        i*WIDTH+WIDTH, j*HEIGHT+HEIGHT, paint);
		            }
		        }
	        }
	        	
	        
	        
			imageView.setImageBitmap(rotated);
			setMarker(-100,-100);
		}
        
	}
	
	private boolean isFocus(String name) {
		for (int i = 0; i < wifiNames.length; i++) {
			if (wifiNames[i].equals(name)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onPick(View v) {

		imageView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				float[] point = new float[2];
				point[0] = event.getX();
				point[1] = event.getY();

                setMarker((int)point[0],(int)point[1]);
                MultiPointTouchListener.isMarkered = true;
                //canvas.drawPoint((int)point[0],(int)point[1],paint);

				Matrix matrix = new Matrix();
				if (imageView.getImageMatrix().invert(matrix)) {
					matrix.mapPoints(point);

					x = point[0] * COMPRESS_RATE;
					y = point[1] * COMPRESS_RATE;
                    xx = point[0];
                    yy = point[1];
                    
                    
                    MultiPointTouchListener.point[0] = xx;
                    MultiPointTouchListener.point[1] = yy;
                    text.setText("X:" + x + "|Y:" + y);
				} else {
					text.setText("Matrix is not invertible?");
				}

				imageView.setOnTouchListener(mtpl);
				return true;
			}

		});

	}

	
	public void onStart(View v) {
		Toast.makeText(getApplicationContext(), "Collecting Wi-Fi data...", Toast.LENGTH_LONG)
				.show();
//		pickButton.setEnabled(false);
		startButton.setEnabled(false);
		scanCount = 0;
		count = 0;
		signals.clear();
		macMap.clear();
		start_record = true;
		wifiManager.startScan();

        Log.i("wifilock","start");
        wifiLock = createWifiLock("zzy",3);
        lockWifi();
        setMeasuredPoint(xx,yy);
	}

	public void onEnd(View v) {
		produceTxtRecord();
		
	}

    public void onDelete(View v){
        if(!fingerprints.isEmpty()){
            fingerprints.remove(fingerprints.size() - 1);
            Toast.makeText(getApplicationContext(),"Cleanned",Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(getApplicationContext(),"No Records",Toast.LENGTH_LONG).show();
        }
    }

    private void setMeasuredPoint(float x, float y){
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.RED);
        paint.setStrokeWidth(2);
        System.out.println("draw a point");
        canvas.drawPoint(x,y,paint);
    }

    private void setMarker(int x, int y){
		MarginLayoutParams params=(MarginLayoutParams )marker.getLayoutParams();
		params.setMargins(x - MARKER_WIDTH/2,y - MARKER_HEIGHT + 4, 0,0);
        marker.setLayoutParams(params);
    }


	private void produceTxtRecord(){
	
		File file = new File( OUTPUT_BASE_PATH);
        if(!file.exists()){
           file.mkdirs();
        }
        File ff = new File(OUTPUT_BASE_PATH +"fp_"+ FloorConfig.focusedDistrict +".txt");
        try {
            FileWriter fw = new FileWriter(ff,true);
            for (Fingerprint fp : fingerprints) {
                fw.write(fp.areaId+";"+fp.time+";"+fp.p.x+";"+fp.p.y+";"+Arrays.toString(fp.macs)+";"+Arrays.toString(fp.strengths)+"\n");
            }
			fw.flush();
			fw.close();
			Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_LONG).show();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
        fingerprints.clear();
	}


    private class Fingerprint{
		public String time;
		public int areaId;
		public AreaPoint p;
		public String[] macs;
		public double[] strengths;
		public Fingerprint(int index, String time, AreaPoint p,String[] macs, double[] strengths) {
			super();
			this.p = p;
			this.strengths = strengths;
			this.macs = macs;
			this.time = time;
			this.areaId = index;
		}
		
	}

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void exit() {
        if (!isExit) {
            isExit = true;
            Toast toast = Toast.makeText(getApplicationContext(), "再按一次退出程序",
                    Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            finish();
            System.exit(0);
        }
    }
    
    public class RemoteMapRetrieveTask extends AsyncTask<String, Void, String> {
		@Override
        protected String doInBackground(String... arg0) {
			String imgUrl =getString(R.string.IP) + getString(R.string.img_url);
			imgUrl = imgUrl.replace("*areaId*", arg0[0]);

			try {
				 URL imageUrl = new URL(imgUrl);
			     URLConnection ucon = imageUrl.openConnection();

			     InputStream is = ucon.getInputStream();
			     BufferedInputStream bis = new BufferedInputStream(is);

			     ByteArrayBuffer baf = new ByteArrayBuffer(1024);
			     int current = 0;
			     while ((current = bis.read()) != -1) {
			         baf.append((byte) current);
			     }
			     OutputStream out = new BufferedOutputStream(new FileOutputStream(arg0[1]));
			     out.write(baf.toByteArray());
			     out.close();
			     is.close();
			     
			    
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return arg0[0];			
        }

		@Override
		protected void onPostExecute(final String content) {
			setImageView(content);
		}
        
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.area0:
	            System.out.println("OK,area0");
	            changeFocusedDistrict(0);
	            return true;
	        case R.id.area1:
	        	System.out.println("OK,area1");
	        	changeFocusedDistrict(1);
	            return true;
	        case R.id.area2:
	        	System.out.println("OK,area2");
	        	changeFocusedDistrict(2);
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}
    
	
    
}
