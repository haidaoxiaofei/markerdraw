package com.ust.cse.structures;




public class AreaPoint{
	public int areaId;
	public float x;
	public float y;
	public double weight = Double.MAX_VALUE;
	public static final int RADIUS = 16;


	public void setWeight(double weigtht) {
		this.weight = weight;
	}

	public double getWeight() {
		return weight;
	}



	public double distance(AreaPoint p) {
		return Math.sqrt((this.x - p.getX()) * (this.x - p.getX())
				+ (this.y - p.getY()) * (this.y - p.getY()));
	}

	@Override
	public boolean equals(Object obj) {
		return this.x == ((AreaPoint) obj).x && this.y == ((AreaPoint) obj).y;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 11
				* hash
				+ (int) (Double.doubleToLongBits(this.x) ^ (Double
						.doubleToLongBits(this.x) >>> 32));
		hash = 11
				* hash
				+ (int) (Double.doubleToLongBits(this.y) ^ (Double
						.doubleToLongBits(this.y) >>> 32));
		return hash;
	}



	public String getString() {
		return "" + (int) x + " " + (int) y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public boolean isNearTo(AreaPoint p, float radius) {
		return radius > this.distance(p);
	}

	

	

	public enum PointType {
		START, END, MARK;
	}
	public AreaPoint(int x, int y, int areaId) {
		super();
		this.x = x;
		this.y = y;
		this.areaId = areaId;
	}
	public AreaPoint() {
		
		super();
		weight = 0;
	}
	@Override
	public String toString() {
		return "[" + x + ", " + y + ";"+ weight+"]";
	}
}
