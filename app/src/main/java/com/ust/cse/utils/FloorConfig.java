package com.ust.cse.utils;

import android.content.res.AssetManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class FloorConfig{
	public static int focusedDistrict = 0;
	private static FloorConfig instance = new FloorConfig();
	public String districtName;
	public String[] floorNames;
	public String[] floorFolders;
	public String[]	wifiNames;
	public int compress_rate;
	public boolean GRID;
	public float convertDegree;
	
	private FloorConfig(){

	}
	
	public void reloadConfig(InputStream input){
		JSONParser parser = new JSONParser();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    	try {


			JSONArray jArray = (JSONArray) parser.parse(new InputStreamReader(input));
			JSONObject jobj = (JSONObject) jArray.get(focusedDistrict);
			districtName = (String)jobj.get("districtName");
			JSONArray tmpArray = (JSONArray) jobj.get("floorNames");
			List<String> list = new ArrayList<String>();
			for (int i=0; i<tmpArray.size(); i++) {
			    list.add((String)tmpArray.get(i) );
			}
			floorNames = list.toArray(new String[list.size()]);

			tmpArray = (JSONArray) jobj.get("floorFolders");
			list.clear();
			for (int i=0; i<tmpArray.size(); i++) {
			    list.add((String)tmpArray.get(i) );
			}
			floorFolders = list.toArray(new String[list.size()]);
			
			tmpArray = (JSONArray) jobj.get("wifiNames");
			list.clear();
			for (int i=0; i<tmpArray.size(); i++) {
			    list.add((String)tmpArray.get(i) );
			}
			wifiNames = list.toArray(new String[list.size()]);
			
			String tmp = (String)jobj.get("compress_rate");
			compress_rate = Integer.valueOf(tmp);
			
			tmp = (String)jobj.get("grid");
			if (tmp.equals("true")) {
				GRID = true;
			} else {
				GRID = false;
			}
			
			tmp = (String)jobj.get("convertDegree");
			convertDegree = Float.valueOf(tmp);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public static FloorConfig getInstance(){
		return instance;
	}
}
